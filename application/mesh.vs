cbuffer cbPerObject
{
	float4x4 WVP;
};

struct VS_INPUT
{
    float3 pos : POSITION;
    float3 nor : NORMAL;
    float2 tex : TEXCOORD;
};

struct VS_OUTPUT
{
	float4 pos : SV_POSITION;
	float2 tex : TEXCOORD;
};

VS_OUTPUT VS(VS_INPUT input)
{
    VS_OUTPUT output;
	float4 tmp = {1.0f,1.0f,1.0f,1.0f};
	tmp.xyz = input.pos.xyz;
    output.pos = mul(tmp, WVP);
    output.tex = input.tex;

    return output;
}