#include "OBJ.h"


OBJ::OBJ(char *fileNameWithoutFormat)
{
	_objFileName = _bobjFileName = _mtlFileName = _txtFileName = fileNameWithoutFormat;
	_objFileName += ".obj";
	_bobjFileName += ".bobj";
	_mtlFileName += ".mtl";
	_txtFileName += ".txt";
	_numIndex = 0;
	_numVertex = 0;
	Identity();
}
OBJ::~OBJ()
{
	ClearMemory();
	_RELEASE(_pVertexBuffer);
	_RELEASE(_pIndexBuffer);
}
void OBJ::ClearMemory()
{
	if (!_vertices.empty()) { _vertices.clear(); }
	if (!_texCoords.empty()) { _texCoords.clear(); }
	if (!_clearData.empty()) { _clearData.clear(); }
	if (!_normals.empty()) { _normals.clear(); }
	if (!_indices.empty()) { _indices.clear(); }
	if (!_subsets.empty()) { _subsets.clear(); }
}

void OBJ::LoadFromObjFileToMemory()
{
	fstream fin(_objFileName, fstream::in);
	char checkChar;
	VERTEX tmp;

	ClearMemory();

	_subsets.push_back(SUBSET());

	if (fin)
	{
		while (!fin.eof())
		{
			fin >> checkChar;	// ��� ����� ���������� ������������ �������

			// ����������?
			if (checkChar == 'v')
			{
				checkChar = fin.get();
				float x, y, z;
				fin >> x >> y;
				// �������?
				if (checkChar == 'n')
				{
					 fin >> z;
					_normals.push_back(XMFLOAT3(x, y, z*-1));
				}
				// ���������� ����������?
				else if (checkChar == 't')
				{
					_texCoords.push_back(XMFLOAT2(x, y*-1));
				}
				else // ������ �������
				{
					fin >> z;
					_vertices.push_back(XMFLOAT3(x, y, z*-1));
				}
			}
			// �������?
			else if (checkChar == 'f')
			{
				UINT iPosition, iTexCoord, iNormal;
				VERTEX vertex;

				for (UINT iFace = 0; iFace < 3; iFace++)
				{
					ZeroMemory(&vertex, sizeof(VERTEX));

					// OBJ ������ ���������� ������� � ��������� �� 1
					fin >> iPosition;
					vertex._position = _vertices[iPosition - 1];

					if ('/' == fin.peek())
					{
						fin.ignore();

						if ('/' != fin.peek())
						{
							// �������� ���������� ����������
							fin >> iTexCoord;
							vertex._texCoord = _texCoords[iTexCoord - 1];
						}

						if ('/' == fin.peek())
						{
							fin.ignore();

							// �������� �������
							fin >> iNormal;
							vertex._normal = _normals[iNormal - 1];
						}
					}

					//��������� ������� � ������
					int index = -1;
					for (int i = 0; i < _clearData.size(); i++)
					{
						if (memcmp(&_clearData[i], &vertex, sizeof(VERTEX)) == 0)
						{
							index = i;
							break;
						}
					}
					if (index < 0)
					{
						_clearData.push_back(vertex);
						_indices.push_back(_clearData.size() - 1);
					} 
					else
					{
						_indices.push_back(index);
					}
				}
				// ������ ������� ������ ������(����� ��� �� �������)
				DWORD tmp;
				_numIndex = _indices.size();
				tmp = _indices[_numIndex - 1];
				_indices[_numIndex - 1] = _indices[_numIndex - 3];
				_indices[_numIndex - 3] = tmp;
			}
			// ���� ����������� ���� � �����������
			else if (checkChar == 'm')
			{
				fin.ignore(6);
				fin >> _mtlFileName;
			}
			// ���� ���������� ��������
			else if (checkChar == 'u')
			{
				fin.ignore(6);
				string mtlName;
				fin >> mtlName;
				_subsets.back()._material = GetMaterialFromFile(mtlName);
			}
			// ���� ����� ������
			else if (checkChar == 'g')
			{
				if (_indices.size() != 0)
				{
					_subsets.push_back(SUBSET(_indices.size()));
				}
			}
			if (checkChar != '\n')
			{
				fin.ignore(INT_MAX, '\n');
			}
		}
		fin.close();
		_numIndex = _indices.size();
		_numVertex = _clearData.size();
		if (!_vertices.empty()) { _vertices.clear(); }
		if (!_texCoords.empty()) { _texCoords.clear(); }
		if (!_normals.empty()) { _normals.clear(); }
	}
}

void OBJ::CreateBobjFromMemory()
{
	if (_clearData.empty() || _indices.empty() || _subsets.empty())
	{
		return;
	}

	fstream binout(_bobjFileName, fstream::out | fstream::trunc | fstream::binary);
	// ����� �������
	int numSubsets = _subsets.size();
	int numVertices = _clearData.size();
	int numIndices = _indices.size();

	binout.write((char*)&numVertices, sizeof(int));
	binout.write((char*)&numIndices, sizeof(int));
	binout.write((char*)&numSubsets, sizeof(int));

	// ����� �������
	for (auto i = _clearData.begin(); i != _clearData.end(); i++)
	{
		binout.write((char*)&(*i), sizeof(VERTEX));
	}

	// ����� �������
	for (auto i = _indices.begin(); i != _indices.end(); i++)
	{
		binout.write((char*)&(*i), sizeof(DWORD));
	}

	// ����� ������
	for (auto i = _subsets.begin(); i != _subsets.end(); i++)
	{
		binout.write((char*)&(*i), sizeof(SUBSET));
	}

	binout.close();
}

void OBJ::CreateTXTFromMemory()
{
	if (_clearData.empty() || _indices.empty() || _subsets.empty())
	{
		return;
	}

	fstream fout(_txtFileName, fstream::out | fstream::trunc);
	// ����� �������
	fout << "SubsetsNum = " << _subsets.size() << '\n' << "VerticesNum = " << _numVertex << '\n' << "IndexesNum = " << _numIndex << endl;

	fout << "\nVERTICES" << endl;
	// ����� �������
	int count = 0;
	for (auto i = _clearData.begin(); i != _clearData.end(); i++, count++)
	{
		fout << "--------- " + to_string(count) +" ---------" << endl;
		fout << "p = " << (*i)._position.x << ' ' << (*i)._position.y << ' ' << (*i)._position.z << endl;
		fout << "t = " << (*i)._texCoord.x << ' ' << (*i)._texCoord.y << endl;
		fout << "n = " << (*i)._normal.x << ' ' << (*i)._normal.y << ' ' << (*i)._normal.z << endl;
		fout << "---------------------" << endl;
		fout << endl;
	}

	fout << "\nINDEXES" << endl;
	// ����� �������
	count = 0;
	for (auto i = _indices.begin(); i != _indices.end(); i++, count++)
	{
		if (count % 3 == 0) { fout << endl; }
		fout << (*i) << ' ';
	}

	fout << "\nSUBSETS" << endl;
	// ����� ������
	for (auto i = _subsets.begin(); i != _subsets.end(); i++)
	{
		fout << "---------------------------START SUBSET---------------------------------" << endl;
		fout << "Pos in indexes = " << (*i)._startIndex << endl;
		fout << "Material info\n" << (*i)._material;
		fout << "----------------------------END SUBSET----------------------------------" << endl;
	}

	fout.close();
}

ostream& operator<<(ostream& os, const MATERIAL& mtl)
{
	os << "AmbientColor = " << mtl._ambientColor.x << ' ' << mtl._ambientColor.y << ' ' << mtl._ambientColor.z << ' ' << mtl._ambientColor.w << endl;
	os << "DiffuseColor = " << mtl._diffuseColor.x << ' ' << mtl._diffuseColor.y << ' ' << mtl._diffuseColor.z << ' ' << mtl._diffuseColor.w << endl;
	os << "SpecularColor = " << mtl._specularColor.x << ' ' << mtl._specularColor.y << ' ' << mtl._specularColor.z << ' ' << mtl._specularColor.w << endl;
	os << "SpecularPower = " << mtl._specularPower << endl;
	os << "IlluminationModel = " << mtl._illuminationModel << endl;
	os << "AmbientTextureMap = " << mtl._ambientTextureMap << endl;
	os << "DiffuseTextureMap = " << mtl._diffuseTextureMap << endl;
	os << "SpecularColorTextureMap = " << mtl._specularColorTextureMap << endl;
	os << "SpecularHighLightComponent = " << mtl._specularHighLightComponent << endl;
	os << "AlphaTextureMap = " << mtl._alphaTextureMap << endl;
	os << "BumpMap = " << mtl._bumpMap << endl;
	os << "DisplacementMap = " << mtl._displacementMap << endl;
	os << "StencilDecalTexture = " << mtl._stencilDecalTexture << endl;
	return os;
}

MATERIAL OBJ::GetMaterialFromFile(string mtlName)
{
	MATERIAL newMaterial;
	ifstream fin(_mtlFileName);
	char checkChar;

	if (fin)
	{
		// ������ � ����� ������ ��� ��������
		string tmpStr = "";
		while (getline(fin, tmpStr))
		{
			if (tmpStr.find(mtlName) != string::npos)
			{
				break;	// ����� ��������� ������
			}
		}

		while (fin)
		{
			fin >> checkChar;	// ��� ����� ���������� ������������ �������

			switch (checkChar)
			{
			case 'K':	// ��� Ka, Kd, Ks
				checkChar = fin.get();
				fin.ignore(1);
				if (checkChar == 'a') { fin >> newMaterial._ambientColor.x >> newMaterial._ambientColor.y >> newMaterial._ambientColor.z; }
				else if (checkChar == 'd') { fin >> newMaterial._diffuseColor.x >> newMaterial._diffuseColor.y >> newMaterial._diffuseColor.z; }
				else if (checkChar == 's') { fin >> newMaterial._specularColor.x >> newMaterial._specularColor.y >> newMaterial._specularColor.z; }
				break;
			case 'N':	// ��� Ns
				if (fin.peek() == 's')
				{
					fin.ignore(2);
					fin >> newMaterial._specularPower;
				}
				break;
			case 'T':	// ��� Tr
				fin.ignore(2);
				fin >> newMaterial._diffuseColor.w;
				newMaterial._ambientColor.w = newMaterial._specularColor.w = newMaterial._diffuseColor.w;
				break;
			case 'd':	// ��� d, disp, decal
				checkChar = fin.get();
				if (' ' == checkChar) 
				{ 
					fin >> newMaterial._diffuseColor.w;
					newMaterial._diffuseColor.w = 1.0f - newMaterial._diffuseColor.w;
					newMaterial._ambientColor.w = newMaterial._specularColor.w = newMaterial._diffuseColor.w;
				}
				else if ('i' == checkChar) { fin.ignore(3); fin >> newMaterial._displacementMap; }
				else if ('e' == checkChar) { fin.ignore(4); fin >> newMaterial._stencilDecalTexture; }
				break;
			case 'i':	// ��� illum
				fin.ignore(5);
				fin >> newMaterial._illuminationModel;
				break;
			case 'b':	// ��� bump
				fin.ignore(4);
				fin >> newMaterial._bumpMap;
				break;
			case 'm':	// ��� map_Ka, map_Kd, map_Ks, map_Ns, map_d, map_bump
				fin.ignore(3);
				checkChar = fin.get();
				switch (checkChar)
				{
				case 'K':
					checkChar = fin.get();
					fin.ignore(1);
					if ('a' == checkChar) { fin >> newMaterial._ambientTextureMap; }
					else if ('d' == checkChar) { fin >> newMaterial._diffuseTextureMap; }
					else if ('s' == checkChar) { fin >> newMaterial._specularColorTextureMap; }
					break;
				case 'N':
					fin.ignore(2);
					fin >> newMaterial._specularHighLightComponent;
					break;
				case 'd':
					fin.ignore(1);
					fin >> newMaterial._alphaTextureMap;
					break;
				case 'b':
					fin.ignore(4);
					fin >> newMaterial._bumpMap;
					break;
				}
				break;
			case 'n':	// ���� ������� ���� �������� 
				if (fin.peek() == 'e') goto end;
				break;
			default:
				break;
			}
			fin.ignore(INT_MAX, '\n');
		}
		end:
		fin.close();
	}
	return newMaterial;
}

void OBJ::LoadFromBobjFileToMemory()
{
	int numSubsets;
	
	ClearMemory();

	fstream bin(_bobjFileName, fstream::in | fstream::binary);

	if (bin)
	{
		bin.read((char*)&_numVertex, sizeof(int));
		bin.read((char*)&_numIndex, sizeof(int));
		bin.read((char*)&numSubsets, sizeof(int));

		VERTEX tmpVertex;
		for (int i = 0; i < _numVertex; i++)
		{
			bin.read((char*)&tmpVertex, sizeof(VERTEX));
			_clearData.push_back(tmpVertex);
		}

		DWORD tmpIndex;
		for (int i = 0; i < _numIndex; i++)
		{
			bin.read((char*)&tmpIndex, sizeof(DWORD));
			_indices.push_back(tmpIndex);
		}

		SUBSET tmpSubset;
		for (int i = 0; i < numSubsets; i++)
		{
			bin.read((char*)&tmpSubset, sizeof(SUBSET));
			_subsets.push_back(tmpSubset);
		}
	}
}

void OBJ::CreateBuffers(ID3D11Device*& pDevice)
{
	if (_clearData.empty() || _indices.empty())
	{
		return;
	}

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	D3D11_SUBRESOURCE_DATA data;
	ZeroMemory(&data, sizeof(data));
	HRESULT hr = S_OK;

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(VERTEX)* _clearData.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	data.pSysMem = &_clearData[0];
	hr = pDevice->CreateBuffer(&bd, &data, &_pVertexBuffer);
	if (FAILED(hr)) { return; }

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(DWORD)* _indices.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	data.pSysMem = &_indices[0];
	hr = pDevice->CreateBuffer(&bd, &data, &_pIndexBuffer);
	if (FAILED(hr)) { return; }


	if (!_clearData.empty()) { _clearData.clear(); }
	if (!_indices.empty()) { _indices.clear(); }
}

void OBJ::SetBuffers(ID3D11DeviceContext*& pImmediateContext)
{
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	pImmediateContext->IASetVertexBuffers(0, 1, &_pVertexBuffer, &stride, &offset);

	pImmediateContext->IASetIndexBuffer(_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void OBJ::Render(ID3D11DeviceContext*& pImmediateContext)
{
	Translate(0.0f, 0.001f, 0.0f);
	for (int i = 0; i < _subsets.size(); i++)
	{
		pImmediateContext->PSSetShaderResources(0, 1, &_subsets[i]._material._pDiffuseTextureRSV);

		int indexStart = _subsets[i]._startIndex;
		int indexEnd = ((i + 1) < _subsets.size()) ? _subsets[i + 1]._startIndex : (_numIndex);
		int indexDrawAmount = indexEnd - indexStart;
		pImmediateContext->DrawIndexed(indexDrawAmount, indexStart, 0);
	}
}

void OBJ::LoadTextures(ID3D11Device*& pDevice)
{
	for (auto i = _subsets.begin(); i != _subsets.end(); i++)
	{
		wchar_t* textureName = CharToWChar((*i)._material._diffuseTextureMap);
		CreateDDSTextureFromFile(pDevice, textureName, NULL, &(*i)._material._pDiffuseTextureRSV);
	}
}

void OBJ::Translate(float x, float y, float z)
{
	_objMatrix *= XMMatrixTranslation(x, y, z);
}
void OBJ::Rotate(float angle, float x, float y, float z)
{
	XMVECTOR v = XMVectorSet(x, y, z, 0.0f);
	_objMatrix *= XMMatrixRotationAxis(v, angle);
}
void OBJ::Scale(float x, float y, float z)
{
	_objMatrix *= XMMatrixScaling(x, y, z);
}

void OBJ::Identity()
{
	_objMatrix = XMMatrixIdentity();
}

XMMATRIX OBJ::GetMatrix()
{
	return _objMatrix;
}