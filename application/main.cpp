#include "MyRender.h"

int main()
{
	Framework framework;

	MyRender *render = new MyRender();

	FrameworkDesc descFramework;
	descFramework._pRender = render;
	
	framework.Init(descFramework);

	framework.Run();

	framework.Close();

	return 0;
}