#pragma once

#include "D3D11_Framework.h"
#include <DirectXMath.h>
#include <d3dcompiler.h>
#include "DDSTextureLoader.h"
#include "OBJ.h"

using namespace DirectX;
using namespace D3D11Framework;

class MyRender : public Render
{
public:
	MyRender();
	bool Init(HWND hwnd);
	bool Draw();
	void Close();
private:
	ID3D11InputLayout* _pLayout;
	ID3D11VertexShader* _pVertexShader;
	ID3D11PixelShader* _pPixelShader;
	ID3D11Buffer* _pConstantBuffer;

	ID3D11ShaderResourceView* _pTexture;
	ID3D11SamplerState* _pSamplerState;
	
	// Состояния рендера
	ID3D11RasterizerState* _pWireFrameRS;
	ID3D11RasterizerState* _pSolidRS;
	ID3D11BlendState* _pTransparency;

	OBJ* _pModel;
	XMMATRIX _world;
	XMMATRIX _view;

	float _rot;
};