#include "MyRender.h"

struct ConstantBuffer
{
	XMMATRIX  WVP;
};

MyRender::MyRender()
{
	_pVertexShader = nullptr;
	_pPixelShader = nullptr;
	_pLayout = nullptr;
	_pConstantBuffer = nullptr;
	
	_pTexture = nullptr;
	_pSamplerState = nullptr;

	_pWireFrameRS = nullptr;
	_pSolidRS = nullptr;
	_pTransparency = nullptr;

	_rot = 0.01;
}

bool MyRender::Init(HWND hwnd)
{
	ID3DBlob *pVSBlob = nullptr;
	HRESULT hr = _compileShaderFromFile(L"mesh.vs", "VS", "vs_4_1", &pVSBlob);
	if (FAILED(hr)) { return false; }
	
	hr = _pDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &_pVertexShader);
	if (FAILED(hr)) { _RELEASE(pVSBlob); return false; }

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	hr = _pDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), &_pLayout);
	_RELEASE(pVSBlob);
	if (FAILED(hr)) { return false; }
	_pImmediateContext->IASetInputLayout(_pLayout);
	_pImmediateContext->VSSetShader(_pVertexShader, NULL, 0);


	ID3DBlob *pPSBlob = nullptr;
	hr = _compileShaderFromFile(L"mesh.ps", "PS", "ps_4_1", &pPSBlob);
	if (FAILED(hr)) { return false; }

	hr = _pDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &_pPixelShader);
	_RELEASE(pPSBlob);
	if (FAILED(hr)) { return false; }
	_pImmediateContext->PSSetShader(_pPixelShader, NULL, 0);

	_pModel = new OBJ("doorman");
	//_pModel->LoadFromObjFileToMemory();
	//_pModel->CreateBobjFromMemory();
	_pModel->LoadFromBobjFileToMemory();
	//_pModel->CreateTXTFromMemory();
	_pModel->CreateBuffers(_pDevice);
	_pModel->SetBuffers(_pImmediateContext);
	_pModel->LoadTextures(_pDevice);

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = _pDevice->CreateBuffer(&bd, NULL, &_pConstantBuffer);
	if (FAILED(hr)) { return false; }
	_pImmediateContext->VSSetConstantBuffers(0, 1, &_pConstantBuffer);

	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = _pDevice->CreateSamplerState(&sampDesc, &_pSamplerState);
	if (FAILED(hr)) { return false; }
	_pImmediateContext->PSSetSamplers(0, 1, &_pSamplerState);

	_world = XMMatrixIdentity();

	XMVECTOR eye = XMVectorSet(0.0f, 6.0f, -4.0f, 0.0f);
	XMVECTOR at = XMVectorSet(0.0f, 5.0f, 0.0f, 0.0f);
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	_view = XMMatrixLookAtLH(eye, at, up);

	// Состояния рендера
	D3D11_RASTERIZER_DESC desc;
	ZeroMemory(&desc, sizeof(D3D11_RASTERIZER_DESC));
	desc.FillMode = D3D11_FILL_WIREFRAME;
	desc.CullMode = D3D11_CULL_NONE;
	_pDevice->CreateRasterizerState(&desc, &_pWireFrameRS);
	desc.FillMode = D3D11_FILL_SOLID;
	desc.CullMode = D3D11_CULL_BACK;
	_pDevice->CreateRasterizerState(&desc, &_pSolidRS);

	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
	ZeroMemory(&rtbd, sizeof(rtbd));
	rtbd.BlendEnable = true;
	rtbd.SrcBlend = D3D11_BLEND_SRC_COLOR;
	rtbd.DestBlend = D3D11_BLEND_BLEND_FACTOR;
	rtbd.BlendOp = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha = D3D11_BLEND_ONE;
	rtbd.DestBlendAlpha = D3D11_BLEND_ZERO;
	rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask = D3D10_COLOR_WRITE_ENABLE_ALL;
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.RenderTarget[0] = rtbd;
	_pDevice->CreateBlendState(&blendDesc, &_pTransparency);

	return true;
}

bool MyRender::Draw()
{
	XMMATRIX WVP;
	ConstantBuffer cb;

	_rot += .0005f;
	if (_rot > 6.26f){ _rot = 0.0f; }

	_pModel->Identity();
	_pModel->Translate(0.0f, 0.0f, 0.55f);
	_pModel->Rotate(_rot, 0.0f, 1.0f, 0.0f);

	_world = _pModel->GetMatrix();
	WVP = _world * _view * _projection;
	cb.WVP = XMMatrixTranspose(WVP);
	_pImmediateContext->UpdateSubresource(_pConstantBuffer, 0, NULL, &cb, 0, 0);
	_pImmediateContext->RSSetState(_pSolidRS);
	float blendFactor[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	_pImmediateContext->OMSetBlendState(_pTransparency, blendFactor, 0xffffffff);

	_pModel->Render(_pImmediateContext);
	
	_pModel->Identity();
	_pModel->Rotate(-XM_PI, 0.0f, 1.0f, 0.0f);
	_pModel->Translate(0.1f, 0.0f, -0.55f);
	_pModel->Rotate(_rot, 0.0f, 1.0f, 0.0f);

	_world = _pModel->GetMatrix();
	WVP = _world * _view * _projection;
	cb.WVP = XMMatrixTranspose(WVP);
	_pImmediateContext->UpdateSubresource(_pConstantBuffer, 0, NULL, &cb, 0, 0);
	_pImmediateContext->RSSetState(_pWireFrameRS);
	_pImmediateContext->OMSetBlendState(0, 0, 0xffffffff);

	_pModel->Render(_pImmediateContext);
	return true;
}

void MyRender::Close()
{
	_RELEASE(_pWireFrameRS);
	_RELEASE(_pSolidRS);
	_RELEASE(_pTransparency);
	delete _pModel;
}